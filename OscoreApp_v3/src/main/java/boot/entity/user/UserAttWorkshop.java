package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_att_workshop")
public class UserAttWorkshop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_att_id", nullable = false)
    private Long workshopAttId;
    @Column(name = "group_number", nullable = false)
    private Long groupNumber;
    @Column(name = "skill_id", nullable = false)
    private Long skillId;
    @Column(name = "skill_score", nullable = false)
    private Long skillScore;

    public UserAttWorkshop() {

    };

    public UserAttWorkshop(Long userId, Long workshopAttId, Long groupNumber, Long skillId, Long skillScore) {
        this.userId = userId;
        this.workshopAttId = workshopAttId;
        this.groupNumber = groupNumber;
        this.skillId = skillId;
        this.skillScore = skillScore;
    };

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWorkshopAttId() {
        return this.workshopAttId;
    }

    public void setWorkshopAttId(Long workshopAttId) {
        this.workshopAttId = workshopAttId;
    }

    public Long getGroupNumber() {
        return this.groupNumber;
    }

    public void setGroupNumber(Long groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public Long getSkillScore() {
        return this.skillScore;
    }

    public void setSkillScore(Long skillScore) {
        this.skillScore = skillScore;
    }

}