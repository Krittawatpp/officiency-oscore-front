package boot.entity;



public class SkillScore {

    private Long skillId;
    private String skillScore;

    public SkillScore() {

    }

    public SkillScore(Long skillId, String skillScore) {
        this.skillId = skillId;
        this.skillScore = skillScore;
    }

    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public String getskillScore() {
        return this.skillScore;
    }

    public void setskillScore(String skillScore) {
        this.skillScore = skillScore;
    }
}