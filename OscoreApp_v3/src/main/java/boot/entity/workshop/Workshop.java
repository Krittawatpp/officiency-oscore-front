package boot.entity.workshop;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "workshop")
public class Workshop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private Long workshopId;
    @Column(name = "create_by_id", nullable = false)
    private Long createById;
    @Column(name = "title", length = 20, nullable = false)
    private String title;
    @Column(name = "description", length = 1200, nullable = true)
    private String description;
    @Column(name = "start", nullable = false)
    private String start;
    @Column(name = "end", nullable = true)
    private Date end;
    @Column(name = "enabled", nullable = false)
    private int enabled;

    public Workshop() {

    }

    // public Workshop(Long createById, String title, String start) {
    // this.createById = createById;
    // this.title = title;
    // this.start = start;
    // }

    // public Workshop(Long createById, String title, String description, String
    // start, Date end) {
    // this.createById = createById;
    // this.title = title;
    // this.description = description;
    // this.start = start;
    // this.end = end;
    // }

    public Workshop(Long workshopId, Long createById, String title, String description, String start, Date end,
            int enabled) {
        this.workshopId = workshopId;
        this.createById = createById;
        this.title = title;
        this.description = description;
        this.start = start;
        this.end = end;
        this.enabled = enabled;
    }

    public Long getCreateById() {
        return this.createById;
    }

    public void setCreateById(Long createById) {
        this.createById = createById;
    }

    public int getEnabled() {
        return this.enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public Long getWorkshopId() {
        return this.workshopId;
    }

    public void setWorkshopId(Long workshopId) {
        this.workshopId = workshopId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart() {
        return this.start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public Date getEnd() {
        return this.end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

}