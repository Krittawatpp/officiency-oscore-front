package boot.entity.workshop;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity

public class WorkshopSkillForReview {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = true)
    private Long id;
    @Column(name = "create_by_id", nullable = false)
    private Long userId;
    @Column(name = "workshop_id", nullable = false)
    private Long workshopId;
    @Column(name = "skill_id", nullable = false)
    private Long skillId;
    @Column(name = "skill_name", nullable = false)
    private String skillName;

    public WorkshopSkillForReview() {

    }

    public WorkshopSkillForReview(Long id, Long userId, Long workshopId, Long skillId, String skillName) {
        this.id = id;
        this.userId = userId;
        this.workshopId = workshopId;
        this.skillId = skillId;
        this.skillName = skillName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWorkshopId() {
        return this.workshopId;
    }

    public void setWorkshopId(Long workshopId) {
        this.workshopId = workshopId;
    }

    public Long getSkillId() {
        return this.skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public String getSkillName() {
        return this.skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

}