package boot.controller;

import java.util.Collection;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.user.AppUser;
import boot.entity.user.UserAllSkillScore;
import boot.entity.user.UserScoreByWorkshopAtt;
import boot.service.JsonService;
import boot.service.user.AppUserService;
import boot.service.user.UserAllSkillScoreService;
import boot.service.user.UserScoreByWorkshopAttService;
import boot.service.workshop.*;
import boot.util.PasswordUtil;
import boot.entity.workshop.*;

@RestController
@CrossOrigin
public class instructorController {

    @Autowired
    private AppUserService appUserService;
    @Autowired
    private UserAllSkillScoreService userAllSkillScoreService;

    // @Autowired
    // private WorkshopService workshopService;
    // private JsonService jsonService;

    // Prach // CHECK!
    @RequestMapping(value = "/api/getUserAll", method = RequestMethod.GET)
    Collection<AppUser> getUserAll() {
        Collection<AppUser> allUser = appUserService.findAllUser();
        return allUser;
    }

    // แสดง profile ของ user นั้นๆ by 'P' // CHECK!
    @RequestMapping(value = "/api/getUserById/{user_id}")
    AppUser getUserById(@PathVariable("user_id") Long userId) {
        try {
            AppUser user = appUserService.findUserByMyId(userId);
            return user;
        } catch (java.util.NoSuchElementException exception) {
            return null;
        }
    }

    // Done!! // CHECK!
    // แสดง score เฉลี่ยของ user นั้นๆ by 'P'
    // ใช้แสดง score ใน profile
    @RequestMapping(value = "/api/getAllUserScoreById/{user_id}", method = RequestMethod.GET)
    List<UserAllSkillScore> getUserAllSkillScorre(@PathVariable("user_id") Long myId) {
        return userAllSkillScoreService.findUserAllSkillScoreById(myId);
    }

    // TODO เพื่มหน้า show instructor เพื่อ review

    // TODO เพิ่มหน้า review instructor

}