package boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

// @CrossOrigin
// @RestController
// class AngularController {
//     // @RequestMapping(value = "/**/{[path:[^\\.]*}")
//     // public String redirectUi() {
//     // return "forward:index.html";
//     // }

//     @RequestMapping(value = "/.*", method = RequestMethod.GET)
//     public String redirect() {
//         return "index";
//     }

//     // @GetMapping(value = "/studentlogin/{id}")
//     // public String getCustomer(@PathVariable int id) {
//     // return "forward:index.html";
//     // }
//     // @RequestMapping(value = "/student")
// }

@Controller
@RequestMapping("/")
public class angularController {
    @RequestMapping(method = RequestMethod.GET)
    public String Index() {
        return "index.html";
    }
}