package boot.controller;

import java.security.Principal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.user.AppUser;
import boot.service.user.AppUserService;
import boot.util.PasswordUtil;

@RestController
@CrossOrigin
public class loginRegistorController {

    @Autowired
    private AppUserService appUserService;

    // Instructor Register by 'AngryCode'
    // ตรวจสอบข้อมูล input จากผู้ใช้ก่อนเก็บลงฐานข้อมูล และส่งกลับข้อมูลผู้ใช้ให้กับ
    // frontend
    @PostMapping(path = "/registerInstructor")
    public ResponseEntity<AppUser> registerInstructor(@RequestParam("email") String email,
            @RequestParam("firstname") String firstName, @RequestParam("lastname") String lastName,
            @RequestParam("tel") String tel, @RequestParam("password") String password,
            @RequestParam("confirmpassword") String confirmPassword) {

        
        boolean validateResult = appUserService.validateInstructor(email, firstName, lastName, tel, password, confirmPassword);

        System.out.println(" ********** Instructor Validation Result : " + validateResult);

        AppUser user = new AppUser();

        if (validateResult == true) {
            
            String passEncrypted = new PasswordUtil().encryptPassword(password);
            String role = "instructor";

            user = appUserService.createNewInstructor(email, firstName, lastName, tel, passEncrypted, role);
            return new ResponseEntity<>(user, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

    }

    // Student Register by 'AngryCode'
    // ตรวจสอบข้อมูล input จากผู้ใช้ก่อนเก็บลงฐานข้อมูล และส่งกลับข้อมูลผู้ใช้ให้กับ
    // frontend
    @PostMapping(path = "/registerStudent")
    public ResponseEntity<AppUser> registerStudent(@RequestParam("email") String email, @RequestParam("password") String password) {

        AppUser student;
        boolean validateResult = appUserService.validateStudent(email, password);
        System.out.println(" ********** Student Register Result : " + validateResult);

        if (validateResult == true) {

            String passEncrypted = new PasswordUtil().encryptPassword(password);
            String role = "student";

            student = appUserService.createNewStudent(email, passEncrypted, role);
            return new ResponseEntity<>(student, HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        }
    }

    @PostMapping(path = "/login")
    public ResponseEntity<AppUser> login(@RequestParam("email") String email, @RequestParam("password") String password) {

        AppUser user;
        ResponseEntity<AppUser> responseUser;
        boolean validateResult = appUserService.validateStudent(email, password);
        System.out.println(" ********** Login Validation Result : " + validateResult);

        // TODO: Enable "j_spring_security_check" for encrypted password security
        if (validateResult == true) {
            user = appUserService.findUserByEmail(email);
            // String passEncrypted = new PasswordUtil().encryptPassword(password);
            // System.out.println("***** Login User Info Email:"+user.getEmail()+"
            // EncryptPassword: "+user.getPassword());
            // System.out.println("PasswordEncrypt: "+passEncrypted);
            // System.out.println("Debug: "+ user.getPassword().equals(passEncrypted));
            System.out.println("******* LOGIN SUCCESSFUL ********");
            responseUser = new ResponseEntity<>(user, HttpStatus.OK);

        } else {
            responseUser = new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        return responseUser;
    }
}
