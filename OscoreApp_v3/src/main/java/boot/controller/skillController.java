package boot.controller;

import java.util.ArrayList;
import java.util.Collection;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mysql.cj.xdevapi.JsonArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties.Lettuce;
import org.springframework.boot.json.JsonParseException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.Skill;
import boot.entity.SkillScore;
import boot.repository.SkillRepository;
import boot.service.SkillService;
import boot.service.user.UserAttWorkshopService;

@RestController
@CrossOrigin
public class skillController {
    @Autowired
    private SkillService skillService;
    @Autowired
    private UserAttWorkshopService userAttWorkshopService;


    // ใช้ดึงข้อมูล skill ทั้งหมด
    @RequestMapping(value = "api/getAllSkill", method = RequestMethod.GET)
    Collection<Skill> getAllSkill() {
        return skillService.findAllSkill();
    }

    @RequestMapping(value = "/skills/{user_id}/{workshop_id}/{group_number}", method = RequestMethod.POST)
    void saveFriendSkillScore(
        @PathVariable("user_id") Long userId,
        @PathVariable("workshop_id") Long workshopAttId,
        @PathVariable("group_number") Long groupNumber, 
        @RequestParam("skill1") String skill1,
        @RequestParam("skill2") String skill2, 
        @RequestParam("skill3") String skill3,
        @RequestParam("skill4") String skill4, 
        @RequestParam("jsonData") String jsonData,
        @RequestParam("skIDs") String skIds ) throws Exception {

        System.err.println("SkillController: UserId:" + userId + " workshopAttId:" + workshopAttId + " groupNumber:" + groupNumber);
        System.err.println("Skill1 "+skill1);
        // System.err.println("SkillController: Feedback: "+feedback);
        System.err.println("JsonData "+jsonData);
        System.err.println("Skill ids " + skIds);

        ArrayList<String> allSkills = new ArrayList<String>();
        allSkills.add(skill1);
        allSkills.add(skill2);
        allSkills.add(skill3);
        allSkills.add(skill4);

                for (int i = 0; i < allSkills.size(); i++) {
                    ObjectMapper mapper = new ObjectMapper();
                    SkillScore skScore = mapper.readValue(allSkills.get(i), SkillScore.class);
                    System.out.println("************Skill ID: "+skScore.getSkillId()+" Skill Score: "+skScore.getskillScore() );
                    userAttWorkshopService.addUserScoreFromReview( userId, workshopAttId, groupNumber, skScore.getSkillId(), Long.parseLong(skScore.getskillScore()) );
                }

        }

}
