package boot.controller;

import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import boot.entity.user.UserAttWorkshop;
import boot.entity.user.UserFeedback;
import boot.entity.user.UserGroup;
import boot.entity.user.UserScoreByWorkshopAtt;
import boot.entity.workshop.Workshop;
import boot.repository.user.UserAttWorkshopRepository;
import boot.repository.user.UserFeedbackRepository;
import boot.service.user.UserAttWorkshopService;
import boot.service.user.UserFeedbackService;
import boot.service.user.UserGroupService;
import boot.service.user.UserScoreByWorkshopAttService;
import boot.service.workshop.WorkshopService;

@RestController
@CrossOrigin
public class studentController {
    @Autowired
    private UserGroupService userGroupService;
    @Autowired
    private UserScoreByWorkshopAttService userScoreByWorkshopAttService;
    @Autowired
    private UserAttWorkshopService userAttWorkshopService;
    @Autowired
    private UserFeedbackService userFeedbackService;
    @Autowired
    private WorkshopService workshopService;

    // ส่งกลับ Workshop ที่นักเรียนและผู้สอนจะเข้าร่วมการประเมิน
    // @RequestMapping(value = "join/{workshop_id}", method = RequestMethod.GET)
    // @ResponseBody
    // Workshop joinWorkshopReview(@PathVariable("workshop_id") Long workshopId) {
    // return workshopService.findWorkshopByWorkshopId(workshopId);
    // }

    // DONE!! by 'P'
    // เพิ่ม group ให้กับ student
    // ใช้หลัง scan QR เพื่อเลือกกลุ่ม
    @RequestMapping(value = "student/addStudentGroup", method = RequestMethod.POST)
    ResponseEntity<String> addStudentGroup(@RequestParam("user_id") Long userId,
            @RequestParam("workshop_id") Long workshopId, @RequestParam("group_number") int groupNumber) {
        UserGroup userGroup;
        ResponseEntity<UserGroup> responseUserGroup;
        int addStudentGroup;

        try {
            addStudentGroup = userGroupService.addUserGroup(userId, workshopId, groupNumber);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + addStudentGroup + "}",
                HttpStatus.OK);
    }

    // Done!! by 'P'
    // แสดง user ใน group นั้นๆ
    // ใช่เพื่อส่งค่าไปเก็บที่ frontend
    @RequestMapping(value = "student/showStudentInGroup/{workshop_id}/{group_number}", method = RequestMethod.GET)
    Collection<UserGroup> getUserInSameGroupByWorkshopIdandGroupNumber(@PathVariable("workshop_id") Long workshopId,
            @PathVariable("group_number") Long groupNumber) {
        try {
            return userGroupService.showUserInSameGroupByWorkshopIdandGroupNumber(workshopId, groupNumber);
        } catch (java.util.NoSuchElementException exception) {
            return null;
        }
    }

    // Done!! by 'P'
    // ใช้เพื่อเพิ่มคะแนนเก็บไปที่่ user_att_workshop
    // ใช้เพื่อเก็บค่าจากหน้า friend review
    @RequestMapping(value = "student/addUserAttWorkshopScore", method = RequestMethod.POST)
    ResponseEntity<String> addUserAttWorkshopScore(@RequestParam("user_id") Long userId,
            @RequestParam("workshop_att_id") Long workshopAttId, @RequestParam("group_number") Long groupNumber,
            @RequestParam("skill_id") Long skillId, @RequestParam("skill_score") Long skillScore) {
        int addUserAttWorkshopScore;
        try {
            addUserAttWorkshopScore = userAttWorkshopService.addUserScoreFromReview(userId, workshopAttId, groupNumber,
                    skillId, skillScore);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + addUserAttWorkshopScore + "}",
                HttpStatus.OK);
    }

    // Done!! by 'P'
    // add feedback กลับไปให้ user_feedback
    // ใช้กับหน้า review friends
    @RequestMapping(value = "student/addUserFeedback", method = RequestMethod.POST)
    public ResponseEntity<String> addUserFeedback(@RequestParam("user_id") Long userId,
            @RequestParam("workshop_attend_id") Long workshopId, @RequestParam("group_number") Long groupNumber,
            @RequestParam("feedback") String feedback, @RequestParam("suggestion") String suggestion) {
        int userFeedback;
        try {
            userFeedback = userFeedbackService.addUserFeedback(userId, workshopId, groupNumber, feedback, suggestion);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"fail\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + userFeedback + "}",
                HttpStatus.OK);
    }

    // Done!! by 'P' and 'AngryCode' // CHECK!!
    // ดึง score ของ user at workshop นั้นๆ
    // ใช้กับหน้า profile score (ตอนจบประเมิน)เฉพาะ workshop ที่เข้าร่วม
    @RequestMapping(value = "/api/userscorebyuseridandworkshopid/{user_id}/{workshop_id}", method = RequestMethod.GET)
    Collection<UserScoreByWorkshopAtt> getUserScoreByUserIdAndWorkshopId(@PathVariable("user_id") Long userId,
            @PathVariable("workshop_id") Long workshopId) {
        // System.err.println("Controller: "+userId + " && "+ workshopId);
        return userScoreByWorkshopAttService.findUserScoreByUserIdandWorkshopId(userId, workshopId);
    }

    // Done!! by 'P'
    // ดึง feedback ของ user at workshop นั้นๆ
    // ใช้กับหน้า profile score (ตอนจบประเมิน)เฉพาะ workshop ที่เข้าร่วม
    @RequestMapping(value = "student/showUserFeedbackForCurrentWorkshop/{user_id}/{workshop_id}", method = RequestMethod.GET)
    Collection<UserFeedback> showUserFeedbackForCurrentWorkshop(@PathVariable("user_id") Long userId,
            @PathVariable("workshop_id") Long workshopId) {
        return userFeedbackService.showUserFeedbackForCurrentWorkshop(userId, workshopId);
    }
}