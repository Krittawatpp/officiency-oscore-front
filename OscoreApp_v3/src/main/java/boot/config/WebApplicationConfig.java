package boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
// import org.springframework.boot.web.servlet.ErrorPage;
// import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;

@Configuration
public class WebApplicationConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
    }

    // @Bean
    // public EmbeddedServletContainerCustomizer containerCustomizer() {
    // return container -> {
    // container.addErrorPages(new ErrrorPage(HttpStatus.NOT_FOND, "/notFound"));
    // };
    // }
}