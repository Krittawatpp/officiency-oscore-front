package boot.service.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.UserFeedback;
import boot.repository.user.UserFeedbackRepository;

@Service
public class UserFeedbackService {
    @Autowired
    UserFeedbackRepository userFeedbackRepository;

    public int addUserFeedback(Long userId, Long workshopId, Long groupNumber, String feedback, String suggestion) {
        return userFeedbackRepository.insertUserFeedback(userId, workshopId, groupNumber, feedback, suggestion);
    }

    public Collection<UserFeedback> showUserFeedbackForCurrentWorkshop(Long userId, Long workshopId) {
        return userFeedbackRepository.showUserFeedbackForCurrentWorkshop(userId, workshopId);
    }
}