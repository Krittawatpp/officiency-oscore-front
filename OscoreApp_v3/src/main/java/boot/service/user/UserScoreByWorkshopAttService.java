package boot.service.user;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.UserScoreByWorkshopAtt;
import boot.repository.user.UserScoreByWorkshopAttRepository;

@Service
public class UserScoreByWorkshopAttService {
    @Autowired
    private UserScoreByWorkshopAttRepository userScoreByWorkshopAttRepository;

    public Collection<UserScoreByWorkshopAtt> findUserScoreByUserIdandWorkshopId(Long userId, Long workshopId) {
        // System.err.println("UserScoreByWorkshopAttService "+userId + " && "+
        // workshopId);
        return userScoreByWorkshopAttRepository.findUserScoreByUserIdAndWorkshopId(userId, workshopId);
    }
}