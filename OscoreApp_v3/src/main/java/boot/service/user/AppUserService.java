package boot.service.user;

import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.AppRole;
import boot.entity.user.AppUser;
import boot.entity.user.UserRole;
import boot.repository.AppRoleRepository;
import boot.repository.UserRoleRepository;
import boot.repository.user.AppUserRepository;
import boot.repository.workshop.WorkshopAttRepository;

@Service
public class AppUserService {
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired 
    private UserRoleRepository userRoleRepository;
    @Autowired 
    private AppRoleRepository appRoleRepository;
    @Autowired
    private WorkshopAttRepository workshopAttRepository;

    private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern
            .compile("^[A-Z0-9._%+-]{3,18}@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_FIRSTNAME_REGEX = Pattern.compile("^[A-Z]{2,18}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_LASTNAME_REGEX = Pattern.compile("^[A-Z]{2,18}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern VALID_TEL_REGEX = Pattern.compile("^[0-9]{9,10}$", Pattern.CASE_INSENSITIVE);
    // Minimum 4 characters, at least one letter and one number:
    private static final Pattern VALID_PASSWORD_REGEX = Pattern.compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}$");
    private static final Pattern VALID_REPEATPASSWORD_REGEX = Pattern
            .compile("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{4,}$");

    public Collection<AppUser> findAllUser() {
        Collection<AppUser> allUser = appUserRepository.myFindAll();
        return allUser;
    }

    public AppUser findByUserName(String name) {
        AppUser appUser = appUserRepository.findByUserName(name);
        return appUser;
    }

    public AppUser findUserByMyId(Long userId) {
        return appUserRepository.findUserByMyId(userId);
    }

    public AppUser findUserByEmail(String email) {
        return appUserRepository.findUserByEmail(email);
    }

    // TODO: ADD Validation Process
    public Boolean validateInstructor(String email, String firstName, String lastName, String tel, String password,
            String confirmPassword) {
        boolean validationSuccess = false;

        Matcher eRegEx = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        Boolean emailOK = eRegEx.find();

        Matcher fnRegEx = VALID_FIRSTNAME_REGEX.matcher(firstName);
        Boolean fNameOK = fnRegEx.find();

        Matcher lnRegEx = VALID_LASTNAME_REGEX.matcher(lastName);
        Boolean lNameOK = lnRegEx.find();

        Matcher telRegEx = VALID_TEL_REGEX.matcher(tel);
        Boolean telOK = telRegEx.find();

        Matcher passRegEx = VALID_PASSWORD_REGEX.matcher(password);
        Boolean passOK = passRegEx.find();

        Matcher conPassRegEx = VALID_REPEATPASSWORD_REGEX.matcher(confirmPassword);
        Boolean cPassOK = conPassRegEx.find();

        //Check if this email has been taken by someone else (already exist in database)
        AppUser existUser = appUserRepository.findUserByEmail(email);
        String emailExist = existUser != null ? existUser.getEmail() : "";

        if (emailExist == "" && emailOK && fNameOK && lNameOK && telOK && passOK && cPassOK) {
            if (passOK.equals(cPassOK)) {
                validationSuccess = true;
                System.out.println("AppUserService: ********* Validation success Email:" + emailOK + " Fname: " + fNameOK + " lnRX:"
                        + lNameOK + " telRX:" + telOK + " PassRX:" + passOK + " cPassOK:" + cPassOK);
            } else {
                System.out.println("AppUserService: ********** Password is not matched! emailRX:" + emailOK + " fnRx:" + fNameOK
                        + " lnRX:" + lNameOK + " telRX:" + telOK + " PassRX:" + passOK + " cPassOK:" + cPassOK);
            }
        } else {
            if(emailExist != "") System.out.println("You can't not use this email:"+emailExist+" as it's been already registered.");
            System.out.println("AppUserService: ********** Validation fail! emailExist:"+emailExist+" emailRX:" + emailOK + " fnRx:" + fNameOK + " lnRX:"
                    + lNameOK + " telRX:" + telOK + " PassRX:" + passOK + " cPassOK:" + cPassOK);
        }

        return validationSuccess;
    }

    public Boolean validateStudent(String email, String password) {
        boolean validationSuccess = false;

        Matcher eRegEx = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        Matcher passRegEx = VALID_PASSWORD_REGEX.matcher(password);
        Boolean emailOK = eRegEx.find();
        Boolean passOK = passRegEx.find();

        //Check if this email has been taken by someone else (already exist in database)
        // AppUser existUser = appUserRepository.findUserByEmail(email);
        // String emailExist = existUser != null ? existUser.getEmail() : "";
        
        // if(emailExist != "") {
        //     System.out.println("validateStudent: This email:"+emailExist+" has benn already taken by somone else.");
        //     return validationSuccess = false;
        // }

        if (emailOK && passOK) {
            validationSuccess = true;
        } else {
            System.out.println("validateStudent: ******* EmailOK: " + emailOK + " PassOK:" + passOK);
            validationSuccess = false;
        }

        return validationSuccess;
    }

    public AppUser createNewInstructor(String email, String firstName, String lastName, String tel,
            String encryptPassword, String userRole) {

        AppUser instructor = new AppUser(email, firstName, lastName, tel, encryptPassword, userRole);
        AppRole role = appRoleRepository.findByRoleName(userRole);
        System.out.println(instructor.getUserId());//จะได้ค่า null

        UserRole userR = new UserRole(instructor, role);

        AppUser newInstructor  = appUserRepository.save(instructor);
        System.out.println(instructor.getUserId());//จะได้ตัวเลข id ของ user โดยอัตโนมัติ
        userRoleRepository.save(userR);//เพิ่ม user role เข้าไปในตาราง user_role
        return newInstructor;
    }

    public AppUser createNewStudent(String email, String encryptPassword, String userRole) {

        AppUser student = new AppUser(email, encryptPassword, userRole);

        AppRole role = appRoleRepository.findByRoleName(userRole);
        System.out.println(student.getUserId());//จะได้ค่า null

        UserRole userR = new UserRole(student, role);

        AppUser newStudent  = appUserRepository.save(student);
        System.out.println(student.getUserId());//จะได้ตัวเลข id ของ user โดยอัตโนมัติ
        userRoleRepository.save(userR);//เพิ่ม user role เข้าไปในตาราง user_role
        return newStudent;
    }

}