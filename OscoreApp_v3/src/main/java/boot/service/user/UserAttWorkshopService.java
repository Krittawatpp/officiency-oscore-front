package boot.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.repository.user.UserAttWorkshopRepository;

@Service
public class UserAttWorkshopService {

    @Autowired
    private UserAttWorkshopRepository userAttWorkshopRepository;

    public int addUserScoreFromReview(Long userId, Long workshopAttId, Long groupNumber, Long skillId, Long skillScore) {
        return userAttWorkshopRepository.insertUserAttWorkshopScore(userId, workshopAttId, groupNumber, skillId,
                skillScore);
    }

    
}