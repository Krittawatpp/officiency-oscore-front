package boot.service.user;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.UserGroup;
import boot.repository.user.UserGroupRepository;

@Service
public class UserGroupService {
    @Autowired
    UserGroupRepository userGroupRepository;

    public int addUserGroup(Long userId, Long workshopId, int groupNumber) {
        return userGroupRepository.addUserGroupByWorkshopAndGroupId(userId, workshopId, groupNumber);
    };

    public Collection<UserGroup> showUserInSameGroupByWorkshopIdandGroupNumber(Long workshopId, Long groupNumber) {
        return userGroupRepository.showUserInSameGroupByWorkshopIdandGroupNumber(workshopId, groupNumber);
    };
}