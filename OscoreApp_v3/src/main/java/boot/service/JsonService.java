package boot.service;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import boot.entity.RegistrationRequest;
import boot.entity.workshop.AddWorkshopRequest;

@Service
public class JsonService {

    public RegistrationRequest parseJsonToRegistrationRequest(String jsonUserObject) {
        RegistrationRequest userRegis = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            userRegis = mapper.readValue(jsonUserObject, RegistrationRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return userRegis;
    }

    // Add workshop
    public AddWorkshopRequest parseJsonToEditWorkshopRequest(String jsonWorkshopObject) {

        AddWorkshopRequest addWorkshop = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            addWorkshop = mapper.readValue(jsonWorkshopObject, AddWorkshopRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return addWorkshop;
    }
}