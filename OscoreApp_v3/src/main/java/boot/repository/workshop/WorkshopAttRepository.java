package boot.repository.workshop;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.workshop.WorkshopAtt;

import org.springframework.data.jpa.repository.Modifying;

public interface WorkshopAttRepository extends CrudRepository<WorkshopAtt, Long> {

    @Query(value = "SELECT * FROM user_att_workshop", nativeQuery = true)
    public Collection<WorkshopAtt> findAllWorkshopAtt();

    @Query(value = "SELECT * FROM user_att_workshop WHERE user_id = ?1", nativeQuery = true)
    public Collection<WorkshopAtt> findAllWorkshopAttById(Long userId);

    // @Transactional
    // @Modifying
    // @Query()
}