package boot.repository.workshop;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.workshop.WorkshopSkillForReview;

public interface WorkshopSkillForReviewRepository extends CrudRepository<WorkshopSkillForReview, Long> {

    @Query(value = "SELECT workshop_skill.id, workshop_skill.workshop_id, workshop_skill.create_by_id, workshop_skill.skill_id, skill.skill_name FROM `workshop_skill`  LEFT JOIN skill ON workshop_skill.skill_id = skill.id WHERE workshop_skill.workshop_id = ?1", nativeQuery = true)
    public Collection<WorkshopSkillForReview> findWorkshopSkillByWorkshopId(Long workshopId);
}