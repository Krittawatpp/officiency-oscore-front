package boot.repository.workshop;

import java.util.Collection;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import boot.entity.workshop.Workshop;

public interface WorkshopRepository extends CrudRepository<Workshop, Long> {

    String sqlForAddWorkshop = "INSERT INTO workshop " + "(create_by_id, title, `start`, enabled) " + "VALUES "
            + "(?1, ?2, ?3, 1);";

    String sqlForReturnWorkshop = "SELECT * FROM workshop " + "WHERE create_by_id = ?1 " + "and title = ?2";

    public Iterable<Workshop> findAll();

    @Query(value = "SELECT * FROM workshop WHERE id =?1", nativeQuery = true)
    public Workshop findWorkshopByWorkshopId(Long workshopId);

    @Query(value = "SELECT * FROM workshop WHERE create_by_id = ?1 AND enabled = 1", nativeQuery = true)
    public Iterable<Workshop> findWorkShopByCreatedId(Long id);

    @Query(value = sqlForReturnWorkshop, nativeQuery = true)
    public Workshop findWorkshopByCreateIdandTitle(Long userId, String title);

    @Transactional
    @Modifying
    @Query(value = sqlForAddWorkshop, nativeQuery = true)
    public int addNewWorkshop(Long userId, String title, String start);

    @Transactional
    @Modifying
    @Query(value = "UPDATE workshop SET title = ?2, description = ?3, `start`= ?4, `end`=?5 WHERE id = ?1", nativeQuery = true)
    public int editWorkshop(Long id, String title, String desc, String start, String end);

    String deleteSql = "UPDATE workshop " + "SET enabled = 0 " + "WHERE id = ?1";

    @Transactional
    @Modifying
    @Query(value = deleteSql, nativeQuery = true)
    public int deleteWorkshop(Long id);

}