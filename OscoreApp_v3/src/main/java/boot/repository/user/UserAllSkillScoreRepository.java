package boot.repository.user;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserAllSkillScore;

public interface UserAllSkillScoreRepository extends CrudRepository<UserAllSkillScore, Integer> {
    @Query(value = "SELECT MIN(user_att_workshop.id) as id, user_att_workshop.user_id, skill.skill_name, AVG(user_att_workshop.skill_score) as avgscore from user_att_workshop left join skill on user_att_workshop.skill_id = skill.id GROUP BY user_att_workshop.user_id,skill.id HAVING user_att_workshop.user_id = ?1 ORDER BY user_att_workshop.user_id;", nativeQuery = true)
    public List<UserAllSkillScore> findUserAllSkillScoreByMyId(Long userId);
}