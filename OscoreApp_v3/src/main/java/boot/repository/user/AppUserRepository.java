package boot.repository.user;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Long> {
    public Iterable<AppUser> findAll();

    @Query(value = "SELECT * FROM user WHERE firstname = ?1", nativeQuery = true)
    public AppUser findByUserName(String name);

    @Query(value = "SELECT * FROM user", nativeQuery = true)
    public Collection<AppUser> myFindAll();

    @Query(value = "SELECT * FROM user WHERE user_id =?1", nativeQuery = true)
    public AppUser findUserByMyId(Long id);

    @Query(value = "SELECT * FROM user WHERE email =?1", nativeQuery = true)
    public AppUser findUserByEmail(String email);

}