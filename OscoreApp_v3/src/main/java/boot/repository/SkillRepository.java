package boot.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.Skill;

public interface SkillRepository extends CrudRepository<Skill, Long> {
    @Query(value = "SELECT * FROM skill", nativeQuery = true)
    public Collection<Skill> allSkill();
}